import React, { Fragment, useState } from 'react';
import uuid from 'uuid/v4';
import PropTypes from 'prop-types';


const Formulario  = ({crearTarea}) => {

//Crear State de tareas
const [tarea, actualizarTarea]= useState({
    titulo:'',
    categoria:'',
    fecha:'',
    hora:'',
    descripcion:'',
   

});
const [error, actualizarError] = useState(false)

//Funcion cuando el usuario escribe en input
const actualizarState = e => {
   actualizarTarea({
       ...tarea,
       [e.target.name] : e.target.value
   })
} 

//extraer los valores
const {titulo, categoria, fecha, hora, descripcion} = tarea;


//Sumbit de la tarea
const submitTarea = e => {
    e.preventDefault();
    
    //validar
    if(titulo.trim()=== ''||categoria.trim()=== ''||fecha.trim()=== ''||hora.trim()=== ''||descripcion.trim()=== ''){
        actualizarError(true);
        return;
    }
    //Eliminar el mensaje previo
    actualizarError(false);

    //asignar un ID
    tarea.id= uuid();
    

    //crear la Tarea
    crearTarea(tarea);

    //Reiniciar el form
    actualizarTarea({
        titulo:'',
        categoria:'',
        fecha:'',
        hora:'',
        descripcion:'',
    })

}
    return (
        <Fragment>
            <h2>Crear Tarea</h2>

            { error ? <p className="alerta-error">Todos los campos son obligatorios</p> : null}
            <form
                onSubmit={submitTarea}
            >
                <label>Título</label>
                <input
                type="text"
                name="titulo"
                className="u-full-width"
                placeholder="Título de tarea"
                onChange={actualizarState}
                value={tarea.titulo}
                />
                <label>Categoría</label>
                <input
                type="text"
                name="categoria"
                className="u-full-width"
                placeholder="Categoría de la Tarea"
                onChange={actualizarState}
                value={tarea.categoria}
                />
                <label>Fecha</label>
                <input
                type="date"
                name="fecha"
                className="u-full-width"
                onChange={actualizarState}
                value={tarea.fecha}
                />
                <label>Hora</label>
                <input
                type="time"
                name="hora"
                className="u-full-width"
                onChange={actualizarState}
                value={tarea.hora}
                />
                <label>Descripción</label>
                <textarea
                className="u-full-width"
                name="descripcion"
                onChange={actualizarState}
                value={tarea.descripcion}
                ></textarea>

                <button
                type="submit"
                className="u-full-width button-primary boton"
                >Agregar tarea</button>
            </form>
        </Fragment>
      );
}

Formulario.propTypes = {
    crearTarea: PropTypes.func.isRequired
}
 
export default Formulario;