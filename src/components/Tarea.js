import React from 'react';
import PropTypes from 'prop-types';

const Tarea = ({ tarea,  eliminarTarea}) => (
    <div className="tarea">
        <p>Titulo <span>{tarea.titulo}</span> </p>
        <p>Categoría <span>{tarea.categoria}</span> </p>
        <p>Fecha: <span>{tarea.fecha}</span> </p>
        <p>Hora: <span>{tarea.hora}</span> </p>
        <p>Descripcion <span>{tarea.descripcion}</span> </p>

    <button
        className="button eliminar u-full-width"
        onClick={() => eliminarTarea(tarea.id)}

    >Eliminar</button>
    </div>
);
Tarea.propTypes = {
    cita: PropTypes.object.isRequired,
    eliminarCita: PropTypes.func.isRequired
}
 
export default Tarea;