import React, { Fragment, useState, useEffect } from 'react';
import Formulario from './components/Formulario'
import Tarea from './components/Tarea'


function App() {

  //Tareas en localStorage
  let tareasIniciales = JSON.parse(localStorage.getItem('tareas'));
  if(!tareasIniciales) {
    tareasIniciales= [];
  }

  //Arreglo de Tareas
  const [tareas, guardarTarea] = useState(tareasIniciales);

  //Use effect
  useEffect( () => {
    if(tareasIniciales){
      localStorage.setItem(('tareas'), JSON.stringify(tareas));
    } else {
      localStorage.setItem('tareas', JSON.stringify([]));
    }
  }, [tareas] );


  // funcion que toma tareas y agrega nueva
  const crearTarea = tarea => {
    guardarTarea([
      ...tareas,
      tarea
    ])
  }
  //Eliminar tarea por ID
  const eliminarTarea =  id =>{
    const nuevasTareas =tareas.filter(tarea=> tarea.id!= id );
    guardarTarea(nuevasTareas);

  }

  //Mensaje segun status de tareas
  const mensaje = tareas.length===0 ? 'No hay tareas' : 'Administra tus tareas'


  return (
    <Fragment>
<h1>Lista de Tareas</h1>

<div className="container">
    <div className="row">
    <div className="one-half column">
        <Formulario
          crearTarea={ crearTarea }
           />
    </div>
    <div className="one-half column">
      <h2>{mensaje}</h2>
      {tareas.map(tarea => (
        <Tarea
        key={tarea.id}
         tarea={tarea}
         eliminarTarea={eliminarTarea}
        />
      ))}
     </div>
    </div>
</div>

    </Fragment>
  

  );
}

export default App;
